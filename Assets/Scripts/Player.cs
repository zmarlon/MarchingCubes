using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    [SerializeField] private Transform _playerCamera = null;
    [SerializeField] private float _sensitivity = 4.0f;
    [SerializeField] private float _walkSpeed = 6.0f;

    private float _pitch = 0.0f;

    private CharacterController _controller = null;
    
    void Start() {
        _controller = GetComponent<CharacterController>();
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
        for (var i = 0; i < 5; i++) {
            for (var j = 0; j < 5; j++) {
                var chunk = new GameObject {
                    name = $"Chunk {i}:{0}:{j}"
                };
                chunk.GetComponent<Transform>().localPosition = new Vector3(i * 16.0f, 0.0f, j * 16.0f);
                var c = chunk.AddComponent<Chunk>();
                c.X = i;
                c.Y = 0;
                c.Z = j;
            }
        }
    }

    void UpdateMovement() {
        var inputDirection = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        inputDirection.Normalize();

        var movement = (transform.forward * inputDirection.y
                        + transform.right * inputDirection.x) * _walkSpeed;

        _controller.Move(movement * Time.deltaTime);


        var jump = Input.GetAxis("Jump");
        _controller.Move(new Vector3(0, jump * _walkSpeed * 0.003f, 0));
    }
    
    void UpdateMouse() {
        var mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        _pitch -= mouseDelta.y * _sensitivity;
        _pitch = Mathf.Clamp(_pitch, -90.0f, 90.0f);

        _playerCamera.localEulerAngles = Vector3.right * _pitch;

        transform.Rotate(Vector3.up * mouseDelta.x * _sensitivity);
    }
    
    void Update() {
        UpdateMouse();
        UpdateMovement();
    }
}