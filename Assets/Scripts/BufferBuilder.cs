﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using UnityEngine.Rendering;

namespace DefaultNamespace {
    public class BufferBuilder {
        private List<Vector3> _vertices = new();
        private List<int> _indices = new();

        public BufferBuilder AddTriangle(Vector3 v0, Vector3 v1, Vector3 v2) {
            var indexOffset = _vertices.Count;
            _vertices.Add(v2);
            _vertices.Add(v1);
            _vertices.Add(v0);

            for (var i = 0; i < 3; i++) {
                _indices.Add(indexOffset + 2 - i);
            }

            return this;
        }
        
        public Mesh Build() {
            var mesh = new Mesh();
            mesh.indexFormat = IndexFormat.UInt32;
            mesh.vertices = _vertices.ToArray();
            mesh.triangles = _indices.ToArray();
            mesh.RecalculateNormals();

            return mesh;
        }
    }
}