using System;
using DefaultNamespace;
using UnityEngine;

public class Chunk : MonoBehaviour {
    public Mesh Mesh;
    private MeshFilter _meshFilter;
    private MeshRenderer _meshRenderer;

    public const int PointsPerAxis = 64;

    private Vector3 _lastNoiseOffset;

    private float[] _data = new float[PointsPerAxis * PointsPerAxis * PointsPerAxis];

    public float GetValue(int x, int y, int z) {
        return _data[z * PointsPerAxis * PointsPerAxis + y * PointsPerAxis + x];
    }

    public void SetValue(int x, int y, int z, float value) {
        _data[z * PointsPerAxis * PointsPerAxis + y * PointsPerAxis + x] = value;
    }

    void SetupMesh() {
        _meshFilter = gameObject.AddComponent<MeshFilter>();
        _meshRenderer = gameObject.AddComponent<MeshRenderer>();
        if (Mesh == null) {
            GenerateMesh();
        }

        _meshFilter.sharedMesh = Mesh;
        _meshRenderer.material = new Material(Shader.Find("Diffuse"));
    }

    private Vector4[] GetPoints() {
        const float GAP_SIZE = 0.25f;

        var result = new Vector4[PointsPerAxis * PointsPerAxis * PointsPerAxis];
        for (var i = 0; i < PointsPerAxis; i++) {
            for (var j = 0; j < PointsPerAxis; j++) {
                for (var k = 0; k < PointsPerAxis; k++) {
                    result[k * PointsPerAxis * PointsPerAxis + j * PointsPerAxis + i] =
                        new Vector4(i * GAP_SIZE, j * GAP_SIZE, k * GAP_SIZE, GetValue(i, j, k));
                }
            }
        }

        return result;
    }

    private float Input(int x, int y, int z) {
        const float NOISE_SCALE = 0.1f;
        return PerlinNoise3D(X * PointsPerAxis + x * NOISE_SCALE, Y * PointsPerAxis + y * NOISE_SCALE, Z * PointsPerAxis + z * NOISE_SCALE);
    }

    void GenerateMesh() {
        var builder = new BufferBuilder();

        const float NOISE_SCALE = 0.1f;
        
        for (var i = 0; i < PointsPerAxis; i++) {
            for (var j = 0; j < PointsPerAxis; j++) {
                for (var k = 0; k < PointsPerAxis; k++) {
                    SetValue(i, j, k, Input(i, j, k));
                }
            }
        }

        var points = GetPoints();
        
        for (var i = 0; i < PointsPerAxis - 1; i++) {
            for (var j = 0; j < PointsPerAxis - 1; j++) {
                for (var k = 0; k < PointsPerAxis - 1; k++) {
                    MarchingCubes.March(builder, points, i, j, k, PointsPerAxis, 0.5f);
                }
            }
        }

        Mesh = builder.Build();
        _meshFilter.sharedMesh = Mesh;
    }

    public int X, Y, Z;
    
    void Start() {
        SetupMesh();
    }

    // Update is called once per frame
    void Update() {
    }
    
    public static float PerlinNoise3D(float x, float y, float z)
    {
        var xy = Mathf.PerlinNoise(x, y);
        var xz = Mathf.PerlinNoise(x, z);
        var yz = Mathf.PerlinNoise(y, z);
        var yx = Mathf.PerlinNoise(y, x);
        var zx = Mathf.PerlinNoise(z, x);
        var zy = Mathf.PerlinNoise(z, y);
 
        return (xy + xz + yz + yx + zx + zy) / 6;
    }
}