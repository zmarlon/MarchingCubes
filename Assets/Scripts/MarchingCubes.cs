﻿using UnityEngine;

namespace DefaultNamespace {
    public static partial class MarchingCubes {
        private static int IndexFromCoord(int pointsPerAxis, int x, int y, int z) {
            return z * pointsPerAxis * pointsPerAxis + y * pointsPerAxis + x;
        }

        private static Vector3 InterpolateVerts(Vector4 v1, Vector4 v2, float isoLevel) {
            var t = (isoLevel - v1.w) / (v2.w - v1.w);
            return new Vector3(v1.x, v1.y, v1.z) + t * (new Vector3(v2.x, v2.y, v2.z) - new Vector3(v1.x, v1.y, v1.z));
        }

        public static void March(BufferBuilder builder, Vector4[] points, int indexX, int indexY, int indexZ,
            int pointsPerAxis, float isoLevel) {
            if (indexX >= pointsPerAxis - 1 || indexY >= pointsPerAxis - 1 || indexZ >= pointsPerAxis - 1) {
                return;
            }

            var cubeCorners = new Vector4[] {
                points[IndexFromCoord(pointsPerAxis, indexX, indexY, indexZ)],
                points[IndexFromCoord(pointsPerAxis, indexX + 1, indexY, indexZ)],
                points[IndexFromCoord(pointsPerAxis, indexX + 1, indexY, indexZ + 1)],
                points[IndexFromCoord(pointsPerAxis, indexX, indexY, indexZ + 1)],
                points[IndexFromCoord(pointsPerAxis, indexX, indexY + 1, indexZ)],
                points[IndexFromCoord(pointsPerAxis, indexX + 1, indexY + 1, indexZ)],
                points[IndexFromCoord(pointsPerAxis, indexX + 1, indexY + 1, indexZ + 1)],
                points[IndexFromCoord(pointsPerAxis, indexX, indexY + 1, indexZ + 1)]
            };

            var cubeIndex = 0;
            if (cubeCorners[0].w < isoLevel) cubeIndex |= 1;
            if (cubeCorners[1].w < isoLevel) cubeIndex |= 2;
            if (cubeCorners[2].w < isoLevel) cubeIndex |= 4;
            if (cubeCorners[3].w < isoLevel) cubeIndex |= 8;
            if (cubeCorners[4].w < isoLevel) cubeIndex |= 16;
            if (cubeCorners[5].w < isoLevel) cubeIndex |= 32;
            if (cubeCorners[6].w < isoLevel) cubeIndex |= 64;
            if (cubeCorners[7].w < isoLevel) cubeIndex |= 128;

            for (var i = 0; Triangulation[cubeIndex][i] != -1; i += 3) {
                var a0 = CornerIndexAFromEdge[Triangulation[cubeIndex][i]];
                var b0 = CornerIndexBFromEdge[Triangulation[cubeIndex][i]];

                var a1 = CornerIndexAFromEdge[Triangulation[cubeIndex][i + 1]];
                var b1 = CornerIndexBFromEdge[Triangulation[cubeIndex][i + 1]];

                var a2 = CornerIndexAFromEdge[Triangulation[cubeIndex][i + 2]];
                var b2 = CornerIndexBFromEdge[Triangulation[cubeIndex][i + 2]];

                builder.AddTriangle(
                    InterpolateVerts(cubeCorners[a0], cubeCorners[b0], isoLevel),
                    InterpolateVerts(cubeCorners[a1], cubeCorners[b1], isoLevel),
                    InterpolateVerts(cubeCorners[a2], cubeCorners[b2], isoLevel)
                );
            }
        }
    }
}